tutoriales
http://hdknr.github.io/docs/django/django.contrib.gis.gdal.html
http://nullege.com/codes/search/django.conf.settings.GDAL_LIBRARY_PATH
https://docs.djangoproject.com/en/dev/ref/contrib/gis/tutorial/

pagina para descargar GDAL dependiendo el la version nuestra version de python y de vc++
primero instalar python y despues checar la version (1500, 1600, 1800, etc..)
GDAL-1.11.0.win32-py2.7.msi
http://www.gisinternals.com/sdk/Download.aspx?file=release-1500-gdal-1-11-mapserver-6-4\GDAL-1.11.0.win32-py2.7.msi

BASE PARA EL PROYECTO
https://bitbucket.org/yohanboniface/umap/src/21122dba4f5abd4871f6755f6c2cf1d745df005e/requirements.txt?at=master
http://umap.fluv.io/es/
http://www.digital-geography.com/umap-easy-open-source-webmap-creation-based-on-osm-leaflet-and-django/

Mis variables de entorno
C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;
C:Program Files\Microsoft SQL Server\110\Tools\Binn\;C:\Program Files (x86)\Common Files\Acronis\SnapAPI\;
C:\Program Files (x86)\vim73;C:\Program Files (x86)\Calibre2\;
C:\Program Files (x86)\Git\cmd;C:\Program Files (x86)\Git\bin;
C:\Program Files\nodejs\;C:\Users\hp777\AppData\Roaming\npm;C:\TDM-GCC-64\bin;

C:\Python27;C:\Python27\Scripts;
C:\Program Files (x86)\GDAL

GDAL_DATA
C:\Program Files (x86)\GDAL\gdal-data


requerimientos hasta el momento
Django	1.6.4	1.6.4
South	0.8.4	0.8.4
Unipath	1.0	1.0
pip	1.5.5	1.5.5
psycopg2	2.5.2	2.5.2
#setuptools	3.4.4	3.6
