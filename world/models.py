#from django.db import models
from django.contrib.gis.db import models


class WorldBorder(models.Model):
    fips = models.CharField(max_length=2)
    iso2 = models.CharField(max_length=2)
    iso3 = models.CharField(max_length=3)
    un = models.IntegerField()
    name = models.CharField(max_length=50)
    area = models.IntegerField()
    pop2005 = models.IntegerField()
    region = models.IntegerField()
    subregion = models.IntegerField()
    lon = models.FloatField()
    lat = models.FloatField()

    #Estpecifico de GeoDjango un campo MultipolygonField
    #y sobreescrito el manager por default de la clase
    #para reemplazarlo con un GeoManager
    mpoly =models.MultiPolygonField()
    objects=models.GeoManager()

    def __unicode__(self):
        return self.name





