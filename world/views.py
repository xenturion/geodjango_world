from django.shortcuts import render
from django.http import HttpResponse


def indexView(request):
    return render(request,'home/index.html')

def mapaView(request):
    return HttpResponse('Aqui se va a ver el mapa')
