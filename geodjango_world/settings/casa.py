from .base import *

DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'geodjango_world',
        'USER':'postgres',
        'PASSWORD':'123',
        'HOST':'localhost',
        'PORT': '5432',
    }
}

GDAL_LIBRARY_PATH = r"C:\Program Files (x86)\GDAL\gdal111.dll"

