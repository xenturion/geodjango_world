from unipath import Path


#BASE_DIR = os.path.dirname(os.path.dirname(__file__))
BASE_DIR = Path(__file__).ancestor(3)
#PROJECT_DIR = Path(BASE_DIR).ancestor(2)
DATA_DIR = BASE_DIR.child('data')
TEMPLATE_DIRS = (BASE_DIR.child('templates'),)

SECRET_KEY = 'x2q%4o9u&(n1b0n)!m%hwb5xf^_kn365x+d+g8pz8qd%si4q!n'

DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
)

THIRD_PARTY_APPS = (
    'south',
)

LOCAL_APPS = (
    'leaflet_storage',
    'world',
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'geodjango_world.urls'

WSGI_APPLICATION = 'geodjango_world.wsgi.application'

LANGUAGE_CODE = 'es-Mx'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = False

USE_TZ = True


STATIC_URL = '/pedosJediondos/'
STATICFILES_DIRS=(BASE_DIR.child('static'),)
