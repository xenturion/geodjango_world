from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'geodjango_world.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', 'world.views.indexView'),
    url(r'^world/', include('world.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'leaflet/', include('leaflet_storage.urls')),
)
